class Shape {
  constructor(size, color) {
    this.size = size;
    this.color = color;
  }
}

export { Shape };