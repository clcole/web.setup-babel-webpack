## Setup a project folder

Create a new project folder and *package.json* file:

```bash
mkdir -p setup-babel-webpack/src
cd setup-babel-webpack
npm init -y
```

## Setup Babel

Install Babel and presets:

```bash
npm install --save-dev @babel/core
npm install --save-dev @babel/preset-env
```

Create a new *.babelrc* configuration file:

```js
{
  "presets": ["@babel/preset-env"]
}
```

## Setup webpack

Install webpack:

```bash
npm install --save-dev webpack 
npm install --save-dev webpack-cli 
```

Install webpack loaders and plugin:

```bash
npm install --save-dev babel-loader 
npm install --save-dev style-loader 
npm install --save-dev css-loader 
npm install --save-dev html-loader 
npm install --save-dev html-webpack-plugin 
```

A webpack development server can be used to launch the application in a
browser on start and refresh the browser after modifications.

Install webpack development server:

```bash
npm install --save-dev webpack-dev-server 
```

Edit the *"script"* command in the *package.json* file:

```js
"scripts": {
  "start": "webpack-dev-server --open --mode development",
  "build": "webpack --mode production"
}
```

Configure webpack to run *.js*, *.css*, and *.html* files through the loaders,
and then generate a new *index.html* file from a template in *src/index.html*.

Create a new *webpack.config.js* file:

```js
const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  // defaults to ./src/index.js
  entry: "./src/index.js",

  // defaults to ./dist/main.js
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'main.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: [
          "style-loader", 
          "css-loader"
        ]
      },
      {
        test: /\.html$/,
        use: {
            loader: "html-loader"
        }
      }
    ]
  },

  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
};
```

## Bundle with webpack

Create a webpack bundle:

```bash
npm run build
```

This will create a new *build/index.html* file and a new *build/main.js* file.

Alternatively, use the webpack development server to launch the application in a browser:

```bash
npm start
```
