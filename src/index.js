import { Shape } from "./Shape";
import './styles.css';

let s = new Shape(200, "orange");
let msg = `New shape created with size ${s.size} and color ${s.color}`;

let h1 = document.createElement("h1");
h1.textContent = msg;
document.body.appendChild(h1);
